#ifndef PROJECT_CONE_H
#define PROJECT_CONE_H

using std::sin;
using std::cos;

class ConeMaker {
public:
    // create cone wight given height and radius from n_traingles number of triangles
    MeshPtr makeCone(float height, float radius, size_t n_triangles) {
        // same like sphere creating from Mesh.cpp
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;

        for (size_t i = 0; i < n_triangles; ++i) {
            auto theta_left = 2 * pi * i / n_triangles;
            auto theta_right = 2 * pi * (i + 1) / n_triangles;
            auto theta_mid = (theta_left + theta_right) / 2;

            // vertices of the triangle
            vertices.emplace_back(0, 0, height);
            vertices.emplace_back(radius * cos(theta_left), radius * sin(theta_left), 0);
            vertices.emplace_back(radius * cos(theta_right), radius * sin(theta_right), 0);

            // normals of the triangle
            normals.emplace_back(cos(theta_mid), sin(theta_mid), radius / height);
            normals.emplace_back(cos(theta_left), sin(theta_left), radius / height);
            normals.emplace_back(cos(theta_right), sin(theta_right), radius / height);
        }

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);

        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        return mesh;
    }

private:
    const float pi = glm::pi<float>();
    };


#endif