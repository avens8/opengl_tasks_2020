#include "./common/Application.hpp"
#include "./common/Mesh.hpp"
#include "./common/ShaderProgram.hpp"

#include "common/Cone.hpp"

#include <iostream>
#include <vector>

struct SampleApplication : public Application {
	std::vector<MeshPtr> _cones;
	ShaderProgramPtr _shader;

	const int NUM_BRANCH_LEVELS = 4;
	const float INITIAL_HEIGHT = 1;
	const float INITIAL_RADIUS = 0.07;

	void makeScene() override {
		Application::makeScene();

		// Добавляем движение камеры стрелочками
		_cameraMover = std::make_shared<FreeCameraMover>();

		auto mat0 = glm::mat4(1);
		generate_cones(_cones, mat0, NUM_BRANCH_LEVELS, INITIAL_HEIGHT, INITIAL_RADIUS);

		//Создаем шейдерную программу
		_shader = std::make_shared<ShaderProgram>("693TsaiData1/shaderNormal.vert", "693TsaiData1/shader.frag");
	}

	void draw() override {
	    // Взято из примеров
		Application::draw();

        //Получаем размеры экрана (окна)
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

        //Устанавливаем порт вывода на весь экран (окно)
		glViewport(0, 0, width, height);

        //Очищаем порт вывода (буфер цвета и буфер глубины)
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Устанавливаем шейдер.
		_shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		//Рисуем
		for (const MeshPtr &cone : _cones) {
			_shader->setMat4Uniform("modelMatrix", cone->modelMatrix());
			cone->draw();
		}
	}

private:
    void generate_cones(std::vector<MeshPtr> &cones, glm::mat4 mat0, size_t level, float height, float radius) {
        if (!level) {
            return;
        }

        ConeMaker coneMaker;
        MeshPtr cone0 = coneMaker.makeCone(height, radius, TRIANGLES_PER_CONE);
        cone0->setModelMatrix(mat0);
        cones.push_back(cone0);

        for (size_t i = 0; i < NUMBER_BRANCHES; ++i) {
            draw_branch(cones, mat0, level, height, radius, i);
        }
    }

    void draw_branch(std::vector<MeshPtr> &cones, glm::mat4 mat0, size_t level, float height, float radius, size_t i) {
        glm::mat4 mat = mat0;
        mat = glm::translate(mat, glm::vec3(0.0f, 0.0f, height * BRANCH_TRUNK_DIST));
        float random01 = (rand() / (float) RAND_MAX) / 10;
        mat = glm::rotate(mat, 2 * pi / NUMBER_BRANCHES * (i + random01), glm::vec3(0.0f, 0.0f, 1.0f));
        mat = glm::rotate(mat, BRANCH_TRUNK_ANGLE, glm::vec3(1.0f, 0.0f, 0.0f));
        generate_cones(cones, mat, level - 1, height * BRANCH_HEIGHT_MULTIPLIER,
                       radius * BRANCH_RADIUS_MULTIPLIER);
	}

    const int TRIANGLES_PER_CONE = 80;
    const float pi = glm::pi<float>();

    const size_t NUMBER_BRANCHES = 5;
    const float BRANCH_RADIUS_MULTIPLIER = 0.35;
    const float BRANCH_TRUNK_ANGLE = pi / 5;
    const float BRANCH_HEIGHT_MULTIPLIER = 0.7;
    const float BRANCH_TRUNK_DIST = 0.4;
};

int main() {
	ImGui::GetIO().IniFilename = nullptr;  // to stop creating imgui.ini

	SampleApplication app;
	app.start();

	return 0;
}
